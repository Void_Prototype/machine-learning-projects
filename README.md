These projects were done in a Machine Learning course provided at the University of Saskatchewan. The content of which reflects the information learned throughout the course.


# Covered Topics

Naive Bayes Classification,

KNN Classification,

Decision Tree Classification,

Modelling/Comparisons,

Bayesian Networks,

Missing-Data Handling,

Kernels (Polynomial, Radial Basis),

KMeans, 

Gaussian Mixture Models,

Principal Component Analysis,

Markov Models and Hidden Markov Models,
